'use strict'

const mongoose = require('mongoose');
const Review = mongoose.model('Review');

exports.get = (req, res, next) => {
    Review
        .find({})
        .then(data => {
            res.status(200).send(data);
        }).catch(e => {
            res.status(400).send(e);
        })
}

exports.post = async (req, res, next) => {
    try {
        var review = new Review();
        const exist = await Review.findOne({
            movie: req.body.movie,
            user: req.body.user
        });
        if (!exist) {
            review.movie = req.body.movie;
            review.user = req.body.user;
            review.review = req.body.review;
            review
                .save()
                .then(x => {
                    res.status(201).send({
                        message: 'Nota cadastrada com sucesso'
                    });
                }).catch(e => {
                    res.status(400).send({
                        message: 'Falha ao cadastrar a nota',
                        data: e
                    });
                });
        } else {
            res.status(400).send({
                message: 'Usuário já deu nota para esse filme'
            });
        }
    } catch (e) {
        res.status(400).send({
            message: 'Falha ao cadastrar a nota'
        });
    }
}