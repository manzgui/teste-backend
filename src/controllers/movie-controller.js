'use strict'

const mongoose = require('mongoose');
const Movie = mongoose.model('Movie');
const Review = mongoose.model('Review');

exports.get = (req, res, next) => {
    Movie
        .find({}, 'title')
        .then(data => {
            res.status(200).send(data);
        }).catch(e => {
            res.status(400).send(e);
        });
}

exports.getByTitle = (req, res, next) => {
    Movie
        .find({
            title: req.params.title
        }, 'title')
        .then(data => {
            res.status(200).send(data);
        }).catch(e => {
            res.status(400).send(e);
        });
}
exports.getByCategory = (req, res, next) => {
    Movie
        .find({
            category: req.params.category
        }, 'title')
        .then(data => {
            res.status(200).send(data);
        }).catch(e => {
            res.status(400).send(e);
        });
}

exports.getByDirector = (req, res, next) => {
    Movie
        .find({
            director: req.params.director
        }, 'title')
        .then(data => {
            res.status(200).send(data);
        }).catch(e => {
            res.status(400).send(e);
        });
}

exports.getDetails = async (req, res, next) => {
    try {
        const movie = await Movie.findById(req.params.id);
        if (movie) {
            const reviews = await Review.find({
                movie: req.params.id
            });
            const avgReview = reviews.reduce((sum, rvw) => sum + rvw.review, 0) / reviews.length;
            const data = {
                title: movie.title,
                category: movie.category,
                director: movie.director,
                avgReview: reviews.length == 0 ? "Sem notas" : avgReview
            }
            res.status(200).send(data);
        } else {
            res.status(500).send({
                message: 'Falha ao procurar detalhes do filme'
            });
        }
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao procurar detalhes do filme'
        });
    }
}

exports.post = (req, res, next) => {
    var movie = new Movie();
    movie.title = req.body.title;
    movie.category = req.body.category;
    movie.director = req.body.director;
    movie
        .save()
        .then(x => {
            res.status(201).send({ message: 'Filme cadastrado com sucesso' });
        }).catch(e => {
            res.status(400).send({
                message: 'Falha ao cadastrar o filme',
                data: e
            });
        });
}