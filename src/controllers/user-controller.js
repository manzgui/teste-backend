'use strict'

const mongoose = require('mongoose');
const User = mongoose.model('User');
const md5 = require('md5')
const authService = require('../services/auth-service')

exports.authenticate = async (req, res, next) => {
    try {
        const user = await User.findOne({
            email: req.body.email,
            password: md5(req.body.password),
            active: true
        });
        if (!user) {
            res.status(404).send({
                message: 'Usuário ou senha inválidos'
            });
            return;
        }
        const token = await authService.generateToken({
            id: user._id,
            name: user.name,
            email: user.email,
            roles: user.roles
        });
        res.status(201).send({
            token: token,
            data: {
                email: user.email,
                name: user.name
            }
        });
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
}

exports.post = (req, res, next) => {
    var user = new User();
    user.name = req.body.name;
    user.email = req.body.email;
    user.password = md5(req.body.password);
    user.roles = req.body.roles;
    user
        .save()
        .then(x => {
            res.status(201).send({
                message: 'Usuário cadastrado com sucesso'
            });
        }).catch(e => {
            res.status(400).send({
                message: 'Falha ao cadastrar o usuário',
                data: e
            });
        });
};

exports.put = (req, res, next) => {
    User
        .findByIdAndUpdate(req.params.id, {
            $set: {
                name: req.body.name,
                email: req.body.email,
                password: md5(req.body.password),
                roles: req.body.roles
            }
        }).then(x => {
            res.status(200).send({
                message: 'Usuário atualizado com sucesso'
            });
        }).catch(e => {
            res.status(400).send({
                message: 'Falha ao atualizar o usuário',
                data: e
            });
        });
}

exports.del = (req, res, next) => {
    User
        .findByIdAndUpdate(req.body.id, {
            $set: {
                active: false
            }
        }).then(x => {
            res.status(200).send({
                message: 'Usuário deletado com sucesso'
            });
        }).catch(e => {
            res.status(400).send({
                message: 'Falha ao deletar o usuário',
                data: e
            });
        });
}