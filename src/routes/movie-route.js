'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/movie-controller');
const authService = require('../services/auth-service')

router.get('/', controller.get);
router.get('/:id', controller.getDetails);
router.get('/title/:title', controller.getByTitle);
router.get('/category/:category', controller.getByCategory);
router.get('/director/:director', controller.getByDirector);
router.post('/', authService.isAdmin, controller.post);

module.exports = router;