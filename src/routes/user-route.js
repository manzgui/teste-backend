'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/user-controller');

router.post('/authenticate', controller.authenticate);
router.post('/', controller.post);
router.put('/:id', controller.put);
router.put('/', controller.del);

module.exports = router;